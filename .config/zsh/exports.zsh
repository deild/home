#!/usr/bin/env zsh

# brew
export HOMEBREW_NO_EMOJI=1
export HOMEBREW_NO_INSECURE_REDIRECT=1
export HOMEBREW_CASK_OPTS="--require-sha --appdir=/Applications"
export HOMEBREW_EDITOR="vim"
# To link Rubies to Homebrew's OpenSSL 1.1
#RUBY_CONFIGURE_OPTS="--with-openssl-dir=$(brew --prefix openssl@1.1)"
RUBY_CONFIGURE_OPTS="--with-openssl-dir=/usr/local/opt/openssl@1.1"
export RUBY_CONFIGURE_OPTS=

# history
export SHELL_SESSION_HISTORY=0
export HISTFILESIZE=999999
export HISTSIZE=999999

# Golang
export GOROOT="/usr/local/opt/go/libexec"
export GOPATH=~/Go
export GO15VENDOREXPERIMENT=1

SHUNIT2_SCRIPTS="/usr/local/opt/shunit2"
export SHUNIT2_SCRIPTS

# Source - http://askubuntu.com/questions/35689/highlight-manpages-syntax
# Less Colors for Man Pages
export LESS_TERMCAP_mb=$'\E[01;31m'       # begin blinking
export LESS_TERMCAP_md=$'\E[01;38;5;74m'  # begin bold
export LESS_TERMCAP_me=$'\E[0m'           # end mode
export LESS_TERMCAP_se=$'\E[0m'           # end standout-mode
export LESS_TERMCAP_so=$'\E[38;5;246m'    # begin standout-mode - info box
export LESS_TERMCAP_ue=$'\E[0m'           # end underline
export LESS_TERMCAP_us=$'\E[04;38;5;146m' # begin underline

export LC_ALL=fr_FR.UTF-8
export LANG=fr_FR.UTF-8

export PATH="/usr/local/opt/ruby/bin:$PATH"
export PKG_CONFIG_PATH="/usr/local/opt/ruby/lib/pkgconfig"
export PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"

GIT_MERGE_AUTOEDIT=no
export GIT_MERGE_AUTOEDIT
