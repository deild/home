" vim:foldmethod=marker:foldlevel=0
" VIM Configuration
" Annule la compatibilite avec l’ancetre Vi
set nocompatible
call plug#begin('~/.vim/plugged')       " Indique un répertoire pour les plugins
Plug 'chriskempson/base16-vim'
Plug 'ledger/vim-ledger'                " Ajout de la coloration syntaxique pour les fichiers ledger
Plug 'tomtom/tlib_vim'                  "
Plug 'godlygeek/tabular'
Plug 'sheerun/vim-polyglot'    " Ajouter de la coloration syntaxique pour un large éventail de types de fichiers
Plug 'ervandew/supertab'
Plug 'dense-analysis/ale'      " un large éventail de linter
Plug 'Yggdroot/indentLine'     " affiche une ligne pour marquer l'indentation
Plug 'airblade/vim-gitgutter'  " Un plugin Vim qui montre les marqueurs git
Plug 'habamax/vim-asciidoctor' " Asciidoctor
Plug 'plasticboy/vim-markdown' " Markdown
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ryanoasis/vim-devicons'
Plug 'z0mbix/vim-shfmt', { 'for': 'sh' }
Plug 'rust-lang/rust.vim'
call plug#end()                         " Initialize plugin system
" Colors {{{
syntax enable           " active le traitement syntaxique
if has('termguicolors')
  set termguicolors
endif
colorscheme base16-gruvbox-dark-medium
" }}}
" Spaces & Tabs {{{
set tabstop=2           " nombre d'espaces visuels par TAB
set expandtab           " utiliser les espaces pour les tabulations
set softtabstop=2       " 2 espace de tabulation
set shiftwidth=2
set modelines=1
filetype indent on
filetype plugin on
set autoindent
set copyindent          " copy the previous indentation on autoindenting
set shiftround          " use multiple of shiftwidth when indenting with '<' and '>'
" }}}
" UI Layout {{{
set number              " iindiquer les numéros de ligne
set showcmd             " show command in bottom bar
set nocursorline        " highlight current line
set wildmenu
set lazyredraw
set showmatch           " higlight matching parenthesis
set scrolloff=3         " Affiche un minimum de 3 lignes autour du curseur (pour le scroll)
" }}}
" Searching {{{
set ignorecase          " Ignore la casse lors d’une recherche
set incsearch           " Surligne les resultats de recherche pendant la saisie
set hlsearch            " Surligne les resultats de recherche
set smartcase           " Si une recherche contient une majuscule, re-active la sensibilite a la casse
" turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>
" }}}
" Folding {{{
set foldmethod=indent   " fold based on indent level
set foldnestmax=10      " max 10 depth
set foldenable          " don't fold files by default on open
" space open/closes folds
nnoremap <space> za
set foldlevelstart=10   " start with fold level of 1
" }}}
" Beep {{{
set visualbell          " Empeche Vim de beeper
set noerrorbells        " Empeche Vim de beeper
" }}}
" Misc {{{
" Active le comportement ’habituel’ de la touche retour en arriere
set backspace=indent,eol,start
" -- gitcommit message
autocmd Filetype gitcommit setlocal spell textwidth=72
"  Clean code function
function! CleanCode()
  %retab                                " Replace tabs with spaces
  %s///eg                               " Turn DOS returns ^M into real returns
  %s=  *$==e                            " Delete end of line blanks
  echo "Cleaned up this mess."
endfunction
nmap <leader>tt :call CleanCode()<cr>
" }}}
" Ledger {{{
au BufNewFile,BufRead *.ldg,*.ledger,*.hledger setf ledger | comp ledger
au FileType ledger noremap { ?^\d<CR>
au FileType ledger noremap } /^\d<CR>
let g:ledger_bin = 'hledger'
let g:ledger_maxwidth = 80
let g:ledger_align_at = 60
let g:ledger_default_commodity = '€'
let g:ledger_decimal_sep = ','
function LedgerAlignAll()
  :%LedgerAlign
endfunction
command LedgerAlignAll call LedgerAlignAll()
" }}}
" Leader Shortcuts {{{
let mapleader = ',' " change the mapleader from \ to ,
" -- Editer/recharger rapidement le fichier vimrc
nmap <silent> <leader>ev :edit $MYVIMRC<CR>
nmap <silent> <leader>sv :source $MYVIMRC<CR>
nnoremap <leader>1 :set number!<CR>
" }}}
" Navigation {{{
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" }}}
" airline {{{
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_theme='base16_gruvbox_dark_hard'
"let g:airline_theme='gruvbox'
let g:airline_powerline_fonts = 1
let g:airline_left_sep=''
let g:airline_left_alt_sep = ''
let g:airline_right_sep=''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.maxlinenr = ''
let g:airline#extensions#ale#enabled = 1
let airline#extensions#ale#error_symbol = ' '
"  
let airline#extensions#ale#warning_symbol = ' '
let airline#extensions#ale#show_line_numbers = 1
let g:airline#extensions#whitespace#checks = 
  \ [ 'indent', 'mixed-indent-file', 'conflicts' ]
let g:airline#extensions#hunks#non_zero_only = 1
let g:airline#extensions#hunks#hunk_symbols = ['+', '~', '-']
let g:airline_section_c = "%F %{&modified?'':''}"
 " }}}
" ale {{{
"let g:ale_echo_msg_error_str = " "
"let g:ale_echo_msg_warning_str = " "
let g:ale_echo_msg_format = '%(code): %%s [%severity% | %linter%]'
" }}}  
" indentLine {{{
let g:indentLine_setColors = 1
let g:indentLine_char_list = ['¦', '┆', '┊','|']
" }}}
" Asciidoctor {{{
" Fold sections, default `0`.
let g:asciidoctor_folding = 1
" Fold options, default `0`.
let g:asciidoctor_fold_options = 1
" Conceal *bold*, _italic_, `code` and urls in lists and paragraphs, default `0`.
"let g:asciidoctor_syntax_conceal = 1
" Highlight indented text, default `1`.
"let g:asciidoctor_syntax_indented = 0
" }}} 
" Mark down {{{
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_conceal = 0
let g:vim_markdown_toml_frontmatter = 1
let g:vim_markdown_json_frontmatter = 1
" }}}
" Shfmt {{{
let g:shfmt_extra_args = '-s -i 2 -bn -ci -sr -kp'
let g:shfmt_fmt_on_save = 1
" }}}
