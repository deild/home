# Make sure that PATH is empty before path_helper runs
if [ -f /etc/profile ]; then
    PATH=""
    source /etc/profile
fi

# don't need to type cd to change directories
setopt autocd autopushd pushdignoredups
autoload -U compinit
compinit -u

# prompt complement
export PS2="%F{#fabd2f}%f "

for file in ~/.config/zsh/{path,exports,aliases,functions,extra,token}.zsh; do
  # shellcheck source=/dev/null
  [ -r "$file" ] && [ -f "$file" ] && . "$file";
done
unset file

[[ ! -f /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]] || source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
[[ ! -f /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh ]] || source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh
